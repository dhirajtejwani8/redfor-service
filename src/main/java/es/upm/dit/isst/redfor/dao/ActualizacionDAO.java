package es.upm.dit.isst.redfor.dao;

import java.util.List;

import es.upm.dit.isst.redfor.model.Actualizacion;

public interface ActualizacionDAO {

	public List<Actualizacion> readActualizaciones(int id);

	public Actualizacion createActualizacion(Actualizacion a);

}
