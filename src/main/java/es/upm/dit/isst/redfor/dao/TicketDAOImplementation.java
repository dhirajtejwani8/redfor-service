package es.upm.dit.isst.redfor.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import es.upm.dit.isst.redfor.model.Ticket;

public class TicketDAOImplementation implements TicketDAO {

	private static TicketDAOImplementation instancia = null;

	private TicketDAOImplementation() {
	}

	public static TicketDAOImplementation getInstance() {
		if (null == instancia)
			instancia = new TicketDAOImplementation();
		return instancia;
	}

	@SuppressWarnings("unchecked")
	@Override
	/**@Transactional*/
	public List<Ticket> readTickets() {
		List<Ticket> tickets = new ArrayList<Ticket>();
		Session session = SessionFactoryService.get().openSession();
		session.beginTransaction();
		tickets.addAll(session.createQuery("from Ticket").list());
		session.getTransaction().commit();
		session.close();
		return tickets;
	}

	@Override
	public Ticket readTicket(int id) {
		Ticket t = null;

		for (Ticket ticket : this.readTickets()) {
			if (ticket.getId() == id) {
				t = ticket;
			}
		}

		return t;
	}

	@Override
	public Ticket createTicket(Ticket t) {
		Session session = SessionFactoryService.get().openSession();
		session.beginTransaction();
		try {
			session.save(t);
		} catch (Exception e) {
			t = null;
		}
		session.getTransaction().commit();
		session.close();
		return t;
	}

	@Override
	public Ticket updateTicket(Ticket t) {
		Session session = SessionFactoryService.get().openSession();
		session.beginTransaction();
		session.saveOrUpdate(t);
		session.getTransaction().commit();
		session.close();
		return t;
	}

}