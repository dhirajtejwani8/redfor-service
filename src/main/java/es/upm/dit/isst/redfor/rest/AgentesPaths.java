package es.upm.dit.isst.redfor.rest;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.upm.dit.isst.redfor.dao.AgenteDAOImplementation;
import es.upm.dit.isst.redfor.model.Agente;

@Path("/agentes")
public class AgentesPaths {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Agente> readAgentes() {
		return AgenteDAOImplementation.getInstance().readAgentes();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createAgente(Agente anew) throws URISyntaxException {
		Agente a = AgenteDAOImplementation.getInstance().createAgente(anew);
		if (a != null) {
			URI uri = new URI("/REDFOR-SERVICE/rest/agentes/" + a.getEmail());
			return Response.created(uri).build();
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response readAgente(@PathParam("id") String id) {
		Agente a = AgenteDAOImplementation.getInstance().readAgente(id);
		if (a == null)
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.ok(a, MediaType.APPLICATION_JSON).build();
	}

	@POST
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAgente(@PathParam("id") String id, Agente aNew) throws URISyntaxException {
		System.out.println("Update request for " + id + " " + aNew.toString());
		Agente aOld = AgenteDAOImplementation.getInstance().readAgente(id);
		if ((aOld == null) || (!(aOld.getEmail() == id))) {
			return Response.notModified().build();
		}
		AgenteDAOImplementation.getInstance().updateAgente(aNew);
		return Response.ok().build();
	}

}
