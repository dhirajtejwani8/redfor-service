package es.upm.dit.isst.redfor.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.upm.dit.isst.redfor.dao.NodoDAOImplementation;
import es.upm.dit.isst.redfor.model.Nodo;
import es.upm.dit.isst.redfor.dao.LecturaDAOImplementation;
import es.upm.dit.isst.redfor.model.Lectura;

@Path("/nodos")
public class NodosLecturasPaths {

	// nodos
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Nodo> readNodos() {
		return NodoDAOImplementation.getInstance().readNodos();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createNodo(Nodo nNew) throws URISyntaxException {
		Nodo n = NodoDAOImplementation.getInstance().createNodo(nNew);
		if (n != null) {
			URI uri = new URI("/REDFOR-SERVICE/rest/nodos/" + n.getId());
			return Response.created(uri).build();
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response readNodo(@PathParam("id") int id) {
		Nodo n = NodoDAOImplementation.getInstance().readNodo(id);
		if (n == null)
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.ok(n, MediaType.APPLICATION_JSON).build();
	}

	// lecturas
	@GET
	@Path("{id}/lecturas")
	@Produces(MediaType.APPLICATION_JSON)
	public Response readLecturas(@PathParam("id") int id) {
		List<Lectura> lecturas = LecturaDAOImplementation.getInstance().readLecturas(id);
		if (lecturas == null)
			return Response.status(Response.Status.NOT_FOUND).build();
		return Response.ok(lecturas, MediaType.APPLICATION_JSON).build();
	}

}
